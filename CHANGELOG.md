# v0.0.3

- Add support for Windows.
- Tweak behavior on MacOS (open maximized instead of fullscreen).


# v0.0.2

- Rebundle 0.0.1 correctly with images.


# v0.0.1

- First version published on PyPI.
