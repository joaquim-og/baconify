# baconify: Bacon Generator

`baconify` is a package inspired by
[this Abstruse Goose comic strip](https://abstrusegoose.com/81).


## Getting baconify

It's recommended to run `baconify` in a virtual environment:

```bash
$ python3 -m venv bacon.env
$ source bacon.env/bin/activate
```

To install `baconify` run:

```bash
(bacon.env) $ pip install baconify
```

## Usage

Start up a Python 3 interpreter (`python3`) and type

```python
import bacon
```

for some bacon goodness.
